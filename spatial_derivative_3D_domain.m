function [ u_x, v_x, w_x, u_y, v_y, w_y, u_z, v_z, w_z ] = spatial_derivative_3D_domain( u, v, w, delta_x )

% evaluate derivatives (Central Difeference)
    delta_y = delta_x;
    delta_z = delta_x;
    
    u_x = zeros(size(u));
    v_x = u_x; w_x = u_x;
    
    size_ccx = size(u,1); size_ccy = size(u,2); size_ccz = size(u,3);
    for i=2:size_ccx-1
        u_x(i,:,:) = (u(i+1,:,:) - u(i-1,:,:))/(2*delta_x);
        v_x(i,:,:) = (v(i+1,:,:) - v(i-1,:,:))/(2*delta_x);
        w_x(i,:,:) = (w(i+1,:,:) - w(i-1,:,:))/(2*delta_x);
    end

    u_y = zeros(size(u));
    v_y = u_y; w_y = u_y;
    for j=2:size_ccy-1
        u_y(:,j,:) = (u(:,j+1,:) - u(:,j-1,:))/(2*delta_y);
        v_y(:,j,:) = (v(:,j+1,:) - v(:,j-1,:))/(2*delta_y);
        w_y(:,j,:) = (w(:,j+1,:) - w(:,j-1,:))/(2*delta_y);
    end

    u_z = zeros(size(u));
    v_z = u_z; w_z = u_z;
    for k=2:size_ccz-1
        u_z(:,:,k) = (u(:,:,k+1) - u(:,:,k-1))/(2*delta_z);
        v_z(:,:,k) = (v(:,:,k+1) - v(:,:,k-1))/(2*delta_z);
        w_z(:,:,k) = (w(:,:,k+1) - w(:,:,k-1))/(2*delta_z);
    end
end

