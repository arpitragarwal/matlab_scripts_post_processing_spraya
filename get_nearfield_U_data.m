function [ U_block, V_block, W_block, unique_ccx_block, unique_ccy_block, unique_ccz_block ] = get_nearfield_U_data( file_location , unique_ccx, unique_ccz, unique_ccy, yz_lines)

    liq_frac=textread(file_location);
    
    for k = 1:size(unique_ccz)
        for j = 1:size(unique_ccy)
            U(:,j,k)=liq_frac(yz_lines(:,j,k),1);
            V(:,j,k)=liq_frac(yz_lines(:,j,k),2);
            W(:,j,k)=liq_frac(yz_lines(:,j,k),3);
        end
    end
    
    indices_y = find(abs(unique_ccy) <= 100e-6);
    indices_x = find(abs(unique_ccx) <= 400e-6);
    %indices_x = 1:length(indices_y);
    U_block = U(indices_x,indices_y, indices_y);
    V_block = V(indices_x,indices_y, indices_y);
    W_block = W(indices_x,indices_y, indices_y);
    unique_ccx_block = unique_ccx(indices_x);
    unique_ccy_block = unique_ccy(indices_y);
    unique_ccz_block = unique_ccz(indices_y);

end
