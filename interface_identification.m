close all;
time_index = 10;
%
alpha_1malpha_slice(time_index,:,:) = alpha_slice(time_index,:,:).*(1-alpha_slice(time_index,:,:));
temp(:,:) = alpha_1malpha_slice(time_index,:,:);

[~,h]= contourf(temp,5);
colorbar()
colormap('Jet')
set(h,'LineColor','none');
colorbar()
colormap('Jet')
title('\alpha(1-\alpha)');


%
laplace_alpha_slice = zeros([1, length(alpha_block), length(alpha_block)]);
for i=2:length(alpha_block)-1
    for j=2:length(alpha_block)-1
        laplace_alpha_slice(time_index,i,j) = alpha_slice(time_index,i+1,j) + alpha_slice(time_index,i-1,j) +alpha_slice(time_index,i,j+1) +alpha_slice(time_index,i,j-1) - 4*alpha_slice(time_index,i+1,j);
    end
end
laplace_alpha_slice = 1/(delta_x^2)*laplace_alpha_slice;
mag_nabla_alpha_slice = abs(laplace_alpha_slice);
temp(:,:) = mag_nabla_alpha_slice(time_index,:,:);
figure()
[~,h]= contourf(unique_ccy_block,unique_ccy_block,temp,20);
colorbar()
colormap('Jet')
set(h,'LineColor','none');
title('\nabla^2\alpha');

figure()
plot(unique_ccy_block,temp(length(alpha_slice)/2, :),'o-')

line_integral = trapz(unique_ccy_block, temp(length(alpha_slice)/2, :));
square_sum = sum(sum(temp(:, :)))*(unique_ccy_block(2) - unique_ccy_block(1))^2;
title('\nabla^2\alpha');


% Thresholding
alpha_min_threshold = 0.001;
alpha_max_threshold = 0.999;
step_function_alpha = zeros([length(alpha_block), length(alpha_block)]);
for i=1:length(alpha_block)
    for j=1:length(alpha_block)
        step_function_alpha(i,j) = (alpha_slice(time_index, i,j)>=alpha_min_threshold) && (alpha_slice(time_index, i,j) <= alpha_max_threshold);
    end
end

figure()
[~,h]= contourf(unique_ccy_block,unique_ccy_block,step_function_alpha,20);
colorbar()
colormap('Jet')
set(h,'LineColor','none');
title('0.001\leq\alpha\leq0.999');




figure()
temp(:,:) = alpha_slice(time_index, :,:);
[C,h]= contourf(unique_ccy_block,unique_ccy_block,temp,20);
colorbar()
colormap('Jet')
set(h,'LineColor','none');
title('\alpha');

%%

% gradient 3 point stencil
grad_alpha_x = zeros([length(alpha_block),length(alpha_block)]);
grad_alpha_y = zeros([length(alpha_block),length(alpha_block)]);
mag_grad_alpha = zeros([length(alpha_block),length(alpha_block)]);
for i=2:length(alpha_block)-1
    for j=2:length(alpha_block)-1
        grad_alpha_x(i,j) = (alpha_slice(time_index,i+1,j) - alpha_slice(time_index,i-1,j))*1/(2*delta_x);
        grad_alpha_y(i,j) = (alpha_slice(time_index,i,j+1) - alpha_slice(time_index,i,j-1))*1/(2*delta_x);
    end
end
mag_grad_alpha = abs(grad_alpha_x.*grad_alpha_x + grad_alpha_y.*grad_alpha_y);
mag_grad_alpha = mag_grad_alpha.^0.5;

figure()
[~,h]= contourf(unique_ccy_block,unique_ccy_block,mag_grad_alpha,20);
colorbar()
colormap('Jet')
set(h,'LineColor','none');
title('\mid\nabla\alpha\mid');

% figure()
% plot(unique_ccy_block,mag_grad_alpha(length(alpha_slice)/2, :),'o-')
% 
% line_integral = trapz(unique_ccy_block, mag_grad_alpha(length(alpha_slice)/2, :))
% square_sum = sum(sum(mag_grad_alpha(:, :)))*(unique_ccy_block(2) - unique_ccy_block(1))^2
% title('\mid\nabla\alpha\mid');

epsilon = 1e-10;
step_mag_grad_alpha = mag_grad_alpha > epsilon;
figure()
[~,h]= contourf(unique_ccy_block,unique_ccy_block,step_mag_grad_alpha,1);
colorbar()
colormap('Jet')
set(h,'LineColor','none');
title('H(\mid\nabla\alpha\mid - \epsilon)');



% gradient 5 point stencil
grad_alpha_x = zeros([length(alpha_block),length(alpha_block)]);
grad_alpha_y = zeros([length(alpha_block),length(alpha_block)]);
for i=3:length(alpha_block)-2
    for j=3:length(alpha_block)-2
        grad_alpha_x(i,j) = (-2*alpha_slice(time_index,i+2,j) + 8*alpha_slice(time_index,i+1,j) - 8*alpha_slice(time_index,i-1,j) + 2*alpha_slice(time_index,i-2,j))*1/(12*delta_x);
        grad_alpha_y(i,j) = (-2*alpha_slice(time_index,i,j+2) + 8*alpha_slice(time_index,i,j+1) - 8*alpha_slice(time_index,i,j-1) + 2*alpha_slice(time_index,i,j-2))*1/(12*delta_x);
    end
end
mag_grad_alpha = abs(grad_alpha_x.*grad_alpha_x + grad_alpha_y.*grad_alpha_y);
mag_grad_alpha = mag_grad_alpha.^0.5;

figure()
[~,h]= contourf(unique_ccy_block,unique_ccy_block,mag_grad_alpha,20);
colorbar()
colormap('Jet')
set(h,'LineColor','none');
title('\mid\nabla\alpha\mid');

% figure()
% plot(unique_ccy_block,mag_grad_alpha(length(alpha_slice)/2, :),'o-')
% 
% line_integral = trapz(unique_ccy_block, mag_grad_alpha(length(alpha_slice)/2, :))
% square_sum = sum(sum(mag_grad_alpha(:, :)))*(unique_ccy_block(2) - unique_ccy_block(1))^2
% title('\mid\nabla\alpha\mid');

epsilon = 1e-10;
step_mag_grad_alpha = mag_grad_alpha > epsilon;
figure()
[~,h]= contourf(unique_ccy_block,unique_ccy_block,step_mag_grad_alpha,1);
colorbar()
colormap('Jet')
set(h,'LineColor','none');
title('H(\mid\nabla\alpha\mid - \epsilon)');