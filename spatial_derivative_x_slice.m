function [ u_x_x_slice, v_x_x_slice, w_x_x_slice, u_y_x_slice, v_y_x_slice, w_y_x_slice, u_z_x_slice, v_z_x_slice, w_z_x_slice ] = spatial_derivative_x_slice( u, v, w, delta_x, x_index )

% evaluate derivatives (Central Difeference)
    delta_y = delta_x;
    delta_z = delta_x;
    
    u_x_x_slice = zeros(size(u,2), size(u,3));
    v_x_x_slice= u_x_x_slice; w_x_x_slice = u_x_x_slice;
    
    size_ccx = size(u,1); size_ccy = size(u,2); size_ccz = size(u,3);
    for i=x_index
        u_x_x_slice(:,:) = (u(i+1,:,:) - u(i-1,:,:))/(2*delta_x);
        v_x_x_slice(:,:) = (v(i+1,:,:) - v(i-1,:,:))/(2*delta_x);
        w_x_x_slice(:,:) = (w(i+1,:,:) - w(i-1,:,:))/(2*delta_x);
    end

    u_y_x_slice = zeros(size(u,2), size(u,3));
    v_y_x_slice = u_y_x_slice; w_y_x_slice = u_y_x_slice;
    for j=2:size_ccy-1
        u_y_x_slice(j,:) = (u(x_index,j+1,:) - u(x_index,j-1,:))/(2*delta_y);
        v_y_x_slice(j,:) = (v(x_index,j+1,:) - v(x_index,j-1,:))/(2*delta_y);
        w_y_x_slice(j,:) = (w(x_index,j+1,:) - w(x_index,j-1,:))/(2*delta_y);
    end

    u_z_x_slice = zeros(size(u,2), size(u,3));
    v_z_x_slice = u_z_x_slice; w_z_x_slice = u_z_x_slice;
    for k=2:size_ccz-1
        u_z_x_slice(:,k) = (u(x_index,:,k+1) - u(x_index,:,k-1))/(2*delta_z);
        v_z_x_slice(:,k) = (v(x_index,:,k+1) - v(x_index,:,k-1))/(2*delta_z);
        w_z_x_slice(:,k) = (w(x_index,:,k+1) - w(x_index,:,k-1))/(2*delta_z);
    end
end
